ARG  SPARQL_ENDPOINT="https://sparql-lkod.rabin.golemio.cz/lkod/query"
ARG  CATALOG_VERSION="1.4.1"

FROM bitnami/nginx:1.20
ARG  SPARQL_ENDPOINT
ARG  CATALOG_VERSION
WORKDIR /app
COPY index.html /app/

USER root
RUN sed -i 's@%SPARQL_ENDPOINT%@'"$SPARQL_ENDPOINT"'@' /app/index.html ; \
    sed -i 's@%CATALOG_VERSION%@'"$CATALOG_VERSION"'@' /app/index.html

USER 1001
